# README #

Aplikace na operační systém Android, nevyžaduje žádná oprávnění.

Aplikace je určena k představení projektu na Proseckých dnech techniky a jejich návštěvníkům.

Představuje jednoduchou hru Piškvorky. Aplikace bude aktualizována i po termínu Proseckých dnů techniky.

Bližší info: [Webové stránky z přednášky](https://ucitel.sps-prosek.cz/~maly/PRG/new/Android/)

[Version] 1.0

Použití pouze k osobním, nebo vzdělávacím účelům.
Autor nenese žádnou odpovědnost za škody způsobené používáním aplikace.