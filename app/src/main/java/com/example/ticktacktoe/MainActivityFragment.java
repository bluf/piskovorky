package com.example.ticktacktoe;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.MotionEventCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment which holds game table view and catches events of user's clicks
 * Base game logic
 */
public class MainActivityFragment extends Fragment {

     /// Actual player who is on turn
    private onTurn0 onTurn = onTurn0.cross; // 0 = X; 1 = O

    /// Enum with players
    private enum onTurn0 {cross, circle}

    /// count of columns in one line of gridView, min is 3
    private int noColumns = 5;

    /// holds all items in table
    private Integer[] mThumbIds;

    private boolean mIsTouchHandeled = false;
    private boolean mIsPointerDown = false;
    private int startPx;
    private int startPy;

    /// Zoom
    private float scale = 1f;

    // View classes
    private View view;
    private RelativeLayout gameView;
    private GridView gridview;

    /// Score
    private int scoreX = 0;
    private int scoreO = 0;

    /// Width of screen
    private int screenWidth;

    /// Padding for columns
    int paddingWidth = 3;
    int paddingHeight = 2;

    /// Class instances
    private Context context;
    private ScaleGestureDetector mScaleDetector;
    private TableUtils tableUtils;
    private ImageAdapter imageAdapter;

    /**
     * Set new value of score from score variables
     */
    //TODO ukončení hry ( getResources().getString(R.string.endGame_message); )
    // Určit počet kol
    // Zjistit, zda byl dohrán daný počet kol
    // Vytvořit zprávu
    // Zobrazit zprávu
    private void setScoreToView()
    {
        TextView textView = (TextView) view.findViewById(R.id.score);
        StringBuilder builder = new StringBuilder();
        if(onTurn == onTurn0.circle)
        {
            builder.append(scoreO);
            builder.append(":");
            builder.append(scoreX);
        } else
        {
            builder.append(scoreX);
            builder.append(":");
            builder.append(scoreO);
        }
        textView.setText(builder.toString());
    }

    public void newGameAlert(String message)
    {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());

        alertBuilder.setMessage(message).setTitle(R.string.endGame_title);


        alertBuilder.setPositiveButton(R.string.newGame_title, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id)
            {
                // User clicked on OK button
                // New game will start
                onTurn = onTurn0.cross;
                scoreX = 0;
                scoreO = 0;
                noColumns = 5;
                setDefaultScale();
                setDefaultScroll();
                createDefaultItems();
                setView();
            }
        });

        alertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id)
            {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = alertBuilder.create();
        dialog.show();
    }
    /**
     * On create View are set default items and user's click handlers
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        context = getActivity();
        view = inflater.inflate(R.layout.fragment_main, container, false);

        /// Set Action bar
        setActionBar(view);
        setHasOptionsMenu(true);

        if (savedInstanceState != null)
        {
            try
            {
                mThumbIds = (Integer[]) savedInstanceState.getSerializable("mThumbIds");
                noColumns = (Integer) savedInstanceState.getSerializable("noColumns");
                Integer[] score = (Integer[]) savedInstanceState.getSerializable("score");
                scoreX = score[0];
                scoreO = score[1];
                String onTurnString = (String) savedInstanceState.get("onTurn");
                if (onTurnString.equals(onTurn0.cross.toString())) onTurn = onTurn0.cross;
                else onTurn = onTurn0.circle;
            }catch (NullPointerException e){ createDefaultItems();}
        }
        else
        {
            createDefaultItems();
        }
        return view;
    }

    /**
     * Inflate layout with items for Action bar menu
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.action_bar_menu, menu);
    }

    /**
     * Event fires after user click menu item
     * Items and actions:
     *  Expand - add more columns to gridView and resize every item
     *  Home - New game
     *  Center - reset scale and position of game board
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle item selection
        switch (item.getItemId()) {

            case R.id.action_expand:
                    expand();
                return true;
            case android.R.id.home:

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

                builder.setMessage(R.string.newGame_message)
                        .setTitle(R.string.newGame_title);
                builder.setPositiveButton(R.string.newGame_title, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                        onTurn = onTurn0.cross;
                        scoreX = 0;
                        scoreO = 0;
                        noColumns = 5;
                        setDefaultScale();
                        setDefaultScroll();
                        createDefaultItems();
                        setView();
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // User cancelled the dialog
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;

            case R.id.action_center:
                setDefaultScroll();

                setDefaultScale();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Adds more columns to gridVIew
     * Updates items list
     * Updates griView
     * Returns if is noColumns bigger than 20
     */
    private void expand()
    {
        if(noColumns > 20)
        {
            Toast.makeText(getActivity(),R.string.sizeLimit_reached,Toast.LENGTH_LONG).show();
            return;
        }
        noColumns += 2;
        tableUtils.noColumns += 2;

        List<Integer> y = new ArrayList<Integer>();
        for (int i = 0; i < noColumns; i++) y.add(R.drawable.white);
        y.add(R.drawable.white);
        y.add(mThumbIds[0]);
        int x = 1;
        for (int c = 1; c < mThumbIds.length; c++)
        {
            if (x == noColumns - 3)
            {
                y.add(mThumbIds[c]);
                y.add(R.drawable.white);
                y.add(R.drawable.white);
                x = 0;
            } else
            {
                y.add(mThumbIds[c]);
                x++;
            }
        }
        y.remove(y.size() - 1);
        for (int i = 0; i < noColumns; i++) y.add(R.drawable.white);

        mThumbIds = y.toArray(new Integer[y.size()]);
        tableUtils.mThumbIds = mThumbIds;
        resizeGameBoard();
        imageAdapter.update(mThumbIds, (screenWidth / noColumns) - paddingWidth, (screenWidth / noColumns) - paddingHeight);

        Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);
        gridview.startAnimation(fadeIn);


    }

    /**
     * Set size of game board so it fits actual device screen without scrollbars
     */
    private void resizeGameBoard()
    {
        gridview.setNumColumns(noColumns);
        gridview.setColumnWidth(screenWidth / noColumns - paddingWidth);

        ViewGroup.LayoutParams layoutParams = gridview.getLayoutParams();
        layoutParams.width = (screenWidth / noColumns-2)*noColumns+noColumns-2;
        layoutParams.height = (screenWidth / noColumns-1)*noColumns+noColumns-2;
        gridview.setLayoutParams(layoutParams);
    }

    /**
     * Set new value of whoIsOnTurn from onTurn variable
     */
    private void setOnTurnToView()
    {
        ImageView imageView = (ImageView) view.findViewById(R.id.toolbar_logo);
        if(onTurn == onTurn0.circle)
        {
            imageView.setImageResource(R.drawable.ic_radio_button_unchecked_white_24dp);
        }
        else
        {
            imageView.setImageResource(R.drawable.ic_clear_white_24dp);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("mThumbIds", mThumbIds);
        outState.putSerializable("score", new Integer[]{scoreX,scoreO} );
        outState.putInt("noColumns", noColumns);
        outState.putString("onTurn", onTurn.toString());
    }

    /**
     * Change scaling of gridview depends on user gesture PinchZoom
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        gameView = (RelativeLayout) view.findViewById(R.id.gameBoard);
        setView();

        mScaleDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
            }
            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return true;
            }
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                scale *= detector.getScaleFactor();
                scale = Math.max(1f, Math.min(scale, 3.5f));
                gridview.setPivotX(detector.getFocusX());
                gridview.setPivotY(detector.getFocusY());
                gridview.setScaleX(scale);
                gridview.setScaleY(scale);
                gridview.invalidate();

                mIsTouchHandeled = true;
                return true;
            }
        });

    }

    /**
     * Create default set of items in game -> table with white images
     */
    private void createDefaultItems()
    {
        List< Integer > y = new ArrayList<>();
        for (int i = 0; i < noColumns*noColumns; i++) y.add( R.drawable.white);

        mThumbIds = y.toArray(new Integer[y.size()]);

    }
    /**
     * This method
     * - check phone resolution
     * - count size of one column in GridView
     * - set GridView adapter
     * - handle click events
     * - base game logic with checking rows, checking winner player and expands base game area
     */
    private void setView()
    {
        setOnTurnToView();
        setScoreToView();
        tableUtils = new TableUtils(noColumns, mThumbIds);

        DisplayMetrics displayMetrics = new DisplayMetrics();

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);

        int orientationPixelSize;

        if(displayMetrics.widthPixels < displayMetrics.heightPixels)
        {
            orientationPixelSize = displayMetrics.widthPixels; // portrait
        }
        else
        {
            int mActionBarSize;
            final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(new int[] { android.R.attr.actionBarSize });
            mActionBarSize = (int) styledAttributes.getDimension(0, 0);
            orientationPixelSize = displayMetrics.heightPixels - mActionBarSize;
        }

        screenWidth = orientationPixelSize;

        gridview = (GridView) view.findViewById(R.id.gridview);
        resizeGameBoard();

        imageAdapter = new ImageAdapter(context, mThumbIds, (screenWidth / noColumns)-paddingWidth, (screenWidth / noColumns)-paddingHeight);//(screenWidth-40)/noColumns+2 to first parameter
        gridview.setAdapter(imageAdapter);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            private int touchedPosition = -1;

            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                if (onTurn == onTurn0.circle && mThumbIds[position] == R.drawable.white)
                {
                    if (position == touchedPosition)
                    {
                        touchedPosition = -1;

                        mThumbIds[position] = R.drawable.ic_panorama_fish_eye_black_24dp;
                        tableUtils.mThumbIds[position] = R.drawable.ic_panorama_fish_eye_black_24dp;

                        onTurn = onTurn0.cross;
                        imageAdapter.update(mThumbIds, position, gridview);
                        if (tableUtils.checkWin(position, R.drawable.ic_panorama_fish_eye_black_24dp))
                        {
                            imageAdapter.updateWinRow(tableUtils.okPositions.toArray(new Integer[tableUtils.okPositions.size()]), gridview);
                            scoreO++;
                        }

                        setOnTurnToView();
                        setScoreToView();
                    } else
                    {
                        imageAdapter.updateForCheck(touchedPosition, gridview, 0);
                        touchedPosition = position;
                        imageAdapter.updateForCheck(position, gridview, 1);
                    }

                } else
                {
                    if (mThumbIds[position] == R.drawable.white)
                    {
                        if (position == touchedPosition)
                        {
                            touchedPosition = -1;
                            mThumbIds[position] = R.drawable.ic_clear_black_24dp;
                            tableUtils.mThumbIds[position] = R.drawable.ic_clear_black_24dp;

                            onTurn = onTurn0.circle;
                            imageAdapter.update(mThumbIds, position, gridview);
                            if (tableUtils.checkWin(position, R.drawable.ic_clear_black_24dp))
                            {
                                imageAdapter.updateWinRow(tableUtils.okPositions.toArray(new Integer[tableUtils.okPositions.size()]), gridview);
                                scoreX++;

                            }
                            setOnTurnToView();
                            setScoreToView();
                        } else
                        {
                            imageAdapter.updateForCheck(touchedPosition, gridview, 0);
                            touchedPosition = position;
                            imageAdapter.updateForCheck(position, gridview, 1);
                        }

                    } else
                    {
                        //Log.d("Wrong move", "wrong move");
                    }
                }
            }
        });


        gridview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                mScaleDetector.onTouchEvent(event);

                final int action = MotionEventCompat.getActionMasked(event);
                //event.getAction() & MotionEvent.ACTION_MASK
                switch (action)
                {
                    case MotionEvent.ACTION_POINTER_DOWN:
                        mIsPointerDown = true;
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        mIsPointerDown = false;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mIsTouchHandeled)
                        {
                            mIsTouchHandeled = false;
                            break;
                        }
                        if (mIsPointerDown) break;

                        if (startPy == 0 || startPx == 0)
                        {
                            startPx = (int) event.getX();
                            startPy = (int) event.getY();
                        } else
                        {
                            int movedX = (int) event.getX() - startPx;
                            int movedY = (int) event.getY() - startPy;

                            int x = gameView.getScrollX() - movedX;
                            int y = gameView.getScrollY() - movedY;

                            if (x >= 800 || x <= -800 || y <= -1300 || y >= 800) break;
                            gameView.setScrollX(gameView.getScrollX() - movedX);
                            gameView.setScrollY(gameView.getScrollY() - movedY);

                        }
                        return true;
                    case MotionEvent.ACTION_UP:
                        startPx = 0;
                        startPy = 0;
                        break;
                }
                return true;
            }
        });

    }
    private void setDefaultScale()
    {
        scale = 1f;
        gridview.setScaleX(scale);
        gridview.setScaleY(scale);
        gridview.invalidate();
    }

    private void setDefaultScroll()
    {
        gameView.setScrollX(0);
        gameView.setScrollY(0);

    }
    public void setActionBar(View view)
    {
        android.support.v7.widget.Toolbar myToolbar = (android.support.v7.widget.Toolbar) view.findViewById(R.id.my_toolbar);
        ((MainActivity) context).setSupportActionBar(myToolbar);

        final android.support.v7.app.ActionBar ab =  ((MainActivity) context).getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_replay_white_24dp);
        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
        ab.setDisplayHomeAsUpEnabled(true);

        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
        ab.setDisplayShowTitleEnabled(false);
    }
}
/*
        if(scoreX+scoreO == endGameLimit)
        {
            String zprava = getResources().getString(R.string.endGame_message);

            if (scoreO > scoreX) zprava+= " 0";
            if (scoreO < scoreX) zprava+= " X";
            if (scoreO == scoreX) zprava = getResources().getString(R.string.endGame_draw);
            newGameAlert(zprava);

        }
 */

