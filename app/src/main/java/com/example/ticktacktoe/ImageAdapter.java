package com.example.ticktacktoe;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import static com.example.ticktacktoe.R.drawable.shape;
import static com.example.ticktacktoe.R.drawable.shape2;
import static com.example.ticktacktoe.R.drawable.shape3;
import static com.example.ticktacktoe.R.drawable.shape4;
import static com.example.ticktacktoe.R.drawable.shape5;
import static com.example.ticktacktoe.R.drawable.shape6;

class ImageAdapter extends BaseAdapter {
    private Context mContext;

    private int width;
    private int height;
    private Integer[] mThumbIds = {};

    public ImageAdapter(Context c, Integer[] mThumbIds,int width, int height)
    {
        mContext = c;
        this.mThumbIds = mThumbIds;

        this.width = width;
        this.height = height;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public void updateWinRow(final Integer[] okPositions,final GridView v)
    {
        ImageView vO;
        for (int position: okPositions)
        {
            vO = (ImageView) v.getChildAt(position - v.getFirstVisiblePosition());
            if(vO == null) continue;
            vO.setBackgroundResource(R.color.LightGreen);

            Animation fadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
            vO.startAnimation(fadeIn);

            fadeIn.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation)
                {

                }

                @Override
                public void onAnimationEnd(Animation animation)
                {
                    //Animation fadeOut = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
                }

                @Override
                public void onAnimationRepeat(Animation animation)
                {
                }
            });
        }

    }
    public void updateForCheck(int position, GridView v, int unsetSet)
    {
        final ImageView vO = (ImageView) v.getChildAt(position - v.getFirstVisiblePosition());
        if(vO == null) return;

        if(unsetSet == 1)
        {
            vO.setBackgroundResource(shape4);
            vO.setImageDrawable(null);
        }
        else
        {
            vO.setImageResource(R.drawable.white);
            vO.setBackgroundResource(shape5);
        }
    }
    //TODO změnit shape (default je shape2)
    public void update( Integer[] mThumbIds, int position, GridView v )
    {
       this.mThumbIds = mThumbIds;
       final ImageView vO = (ImageView) v.getChildAt(position - v.getFirstVisiblePosition());
        if(vO == null) return;
        vO.setImageResource(mThumbIds[position]);
        vO.setBackgroundResource(shape5);
        Animation fadeIn = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        vO.startAnimation(fadeIn);

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation)
            {
            }

            @Override
            public void onAnimationEnd(Animation animation)
            {
                //Animation fadeOut = AnimationUtils.loadAnimation(mContext, R.anim.fade_out);
                // vO.startAnimation(fadeOut);
            }

            @Override
            public void onAnimationRepeat(Animation animation)
            {
            }
        });
    }
    public void update(Integer[] mThumbIds,int width, int height)
    {
        this.mThumbIds = mThumbIds;
        this.width = width;
        this.height = height;
        notifyDataSetChanged();
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView,final ViewGroup parent) {
        ImageView imageView;
       // if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(width, height));
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
           if(mThumbIds[position] == R.drawable.white) imageView.setBackgroundResource(R.color.white);
           else imageView.setBackgroundResource(shape2);
            imageView.setAdjustViewBounds(false);
            imageView.setImageResource(mThumbIds[position]);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((GridView) parent).performItemClick(v, position, 0); // Let the event be handled in onItemClick()

                }
            });
            //imageView.setPadding(20, 2, 2, 2);
        return imageView;
    }

}