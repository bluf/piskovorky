package com.example.ticktacktoe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David Maly on 18.11.2015.
 * Class which handle utils for
 * - checking winner row
 * - getting next item base on given position
 */
class TableUtils {

    // All direction for Tic-tack-toe + diagonals
    public enum Direction {below, upper,toLeft, toRight, upLeft, downLeft, upRight, downRight, HORIZONTAL, VERTICAL,DIAGONALLEFT, DIAGONALRIGHT}
    // Counter which check count of same items in same row
    private int counter = -1;
    // positions of same items in one row for update their background in View -> winning rows could be green
    public List<Integer> okPositions = new ArrayList<>();
    // Count of columns in one line
    public int noColumns = 0;
    // Table content -> X,O
    public Integer[] mThumbIds;

    // count from zero so -1
    public static int inRowToWin = 4;


    public TableUtils(int noColumns, Integer[] mThumbsIds)
    {
        this.noColumns = noColumns;
        this.mThumbIds = mThumbsIds;
    }

    /**
     * This method checking winner row in all directions
     * @param position
     * @param drawable
     * @return
     */
    public boolean checkWin(int position, int drawable)
    {
        checkWinRow(position,drawable, Direction.HORIZONTAL);
        if(counter >= inRowToWin)
        {
            counter = -1;
            return true;
        }
        counter = -1;
        okPositions = new ArrayList<>();

        checkWinRow(position, drawable, Direction.VERTICAL);
        if(counter >= inRowToWin)
        {
            counter = -1;
            return  true;
        }
        counter = -1;
        okPositions = new ArrayList<>();

        checkWinRow(position, drawable, Direction.DIAGONALRIGHT);

        if(counter >= inRowToWin)
        {
            counter = -1;
            return true;
        }
        counter = -1;
        okPositions = new ArrayList<>();

        checkWinRow(position,drawable, Direction.DIAGONALLEFT);

        if(counter >= inRowToWin)
        {
            counter = -1;
            return true;
        }
        counter = -1;
        okPositions = new ArrayList<>();

        return false;

    }
    /**
     * Recursive method which searching for items in same row
     * @param position
     * @param drawable
     * @param direction
     */
    private void checkWinRow(int position, int drawable, Direction direction)
    {
        if(direction == Direction.VERTICAL || direction == Direction.HORIZONTAL || direction == Direction.DIAGONALLEFT || direction == Direction.DIAGONALRIGHT)
        {
            if (mThumbIds[position] == drawable)
            {
                counter++;
                okPositions.add(position);

                if (counter == 4) return;
                int nextInArray;

                switch (direction)
                {
                    case VERTICAL:
                        nextInArray = getNextColumn(position % noColumns, position / noColumns, Direction.below);
                        if (nextInArray > -1) checkWinRow(nextInArray, drawable, Direction.below);

                        nextInArray = getNextColumn(position % noColumns, position / noColumns, Direction.upper);
                        if (nextInArray > -1) checkWinRow(nextInArray, drawable, Direction.upper);
                        break;

                    case HORIZONTAL:
                        nextInArray = getNextColumn(position % noColumns, position / noColumns, Direction.toLeft);
                        if (nextInArray > -1) checkWinRow(nextInArray, drawable, Direction.toLeft);

                        nextInArray = getNextColumn(position % noColumns, position / noColumns, Direction.toRight);
                        if (nextInArray > -1) checkWinRow(nextInArray, drawable, Direction.toRight);
                        break;

                    case DIAGONALRIGHT:

                        nextInArray = getNextColumn(position % noColumns, position / noColumns, Direction.upRight);
                        if (nextInArray > -1) checkWinRow(nextInArray, drawable, Direction.upRight);

                        nextInArray = getNextColumn(position % noColumns, position / noColumns, Direction.downLeft);
                        if (nextInArray > -1) checkWinRow(nextInArray, drawable, Direction.downLeft);
                        break;

                    case DIAGONALLEFT:

                        nextInArray = getNextColumn(position % noColumns, position / noColumns, Direction.upLeft);
                        if (nextInArray > -1) checkWinRow(nextInArray, drawable, Direction.upLeft);

                        nextInArray = getNextColumn(position % noColumns, position / noColumns, Direction.downRight);
                        if (nextInArray > -1) checkWinRow(nextInArray, drawable, Direction.downRight);
                        break;
                }
            }
        }
        else
        {
            if (mThumbIds[position] == drawable)
            {
                counter++;
                okPositions.add(position);

                int nextInArray = getNextColumn(position % noColumns, position / noColumns, direction);
                if (nextInArray > -1) checkWinRow(nextInArray, drawable, direction);
            }
        }
    }

    /**
     * Method which return success or not succes = -1 if next item is reachable in given direction
     * @param xAx
     * @param yAx
     * @param direction
     * @return
     */
    private int getNextColumn(int xAx, int yAx, Direction direction)
    {
        int position;
        switch (direction)
        {
            case below:
                position = yAx*noColumns+noColumns+xAx;
                if(position > mThumbIds.length-1) return -1;
                else return position;
            case upper:
                position = yAx*noColumns-noColumns+xAx;
                if(position > mThumbIds.length-1) return -1;
                else return position;
            case toLeft:
                if(xAx == 0) return -1;
                position = yAx*noColumns+xAx-1;
                if(position > mThumbIds.length-1) return -1;
                else return position;
            case toRight:
                if(xAx == noColumns-1) return -1;
                position = (yAx*noColumns+xAx)+1;
                if(position > mThumbIds.length-1) return -1;
                else return position;
            case upLeft:
                if(xAx == 0) return -1;
                position = yAx*noColumns-noColumns+xAx-1;
                if(position > mThumbIds.length-1) return -1;
                else return position;
            case downLeft:
                if(xAx == 0) return -1;
                position = yAx*noColumns+noColumns+xAx-1;
                if(position > mThumbIds.length-1) return -1;
                else return position;
            case upRight:
                if(xAx == noColumns-1) return -1;
                position = yAx*noColumns-noColumns+xAx+1;
                if(position > mThumbIds.length-1) return -1;
                if(position == 0) return -1;
                else return position;
            case downRight:
                if(xAx == noColumns) return -1;
                position = yAx*noColumns+noColumns+xAx+1;
                if(position > mThumbIds.length-1) return -1;
                else return position;
            default:
                return -1;
        }
    }
}
